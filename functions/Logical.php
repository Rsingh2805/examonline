<?php
	include_once baseUrl('functions/Question.php');
	class Logical extends Question{

		public function show(){
			$string= '<li class="list-group-item">'.
					'<div class="container-fluid">'.
						'<small>Question:</small>'.
						'<h4>'. $this->question. '</h4>'.
					'</div>'.
					'<div class="container-fluid">'.
						'<small>Solution</small>'.
						'<text><pre>'. $this->solution. '</pre></text>'.
					'</div>'.
					'<div class="container-fluid">'.
						'<small>Explanation:</small>'.
						'<textarea class="ckeditor" name="ckeditor">'. htmlspecialchars_decode($this->explanation) . '</textarea>'.
					'</div>'.
				'</li>';
				return $string; 
		}
	}


?>
