<?php
	include_once baseUrl('functions/Question.php');
	class Other extends Question{

		public function show(){
			$solution = json_decode($this->solution);
			$solution_part = "";
			foreach ((array)$solution as $item) {
				if($item->type=='simple'){
					$solution_part.= $item->value .PHP_EOL;
				}else if($item->type=='codemirror'){
					$solution_part.= '<textarea class="code">'.$item->value .'</textarea>';
				}else if($item->type=='ckeditor'){
					$solution_part.= '<textarea class="ckeditor" name="ckeditor">'.$item->value .'</textarea>';
				}
			}
			$string= '<li class="list-group-item">'.
					'<div class="container-fluid">'.
						'<small>Question:</small>'.
						'<h4>'. $this->question. '</h4>'.
					'</div>'.
					'<div class="container-fluid">'.
						'<small>Solution</small>'.
						'<div class="well">'.$solution_part. '</div>'.
					'</div>'.
					'<div class="container-fluid">'.
						'<small>Explanation:</small>'.
						'<pre>'. $this->explanation . '</pre>'.
					'</div>'.
				'</li>';
				return $string; 
		}
	}


?>
