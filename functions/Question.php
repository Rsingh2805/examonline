<?php
abstract class Question{
		public $question;
		public $solution;
		public $explanation;

		function __construct($question_text, $question_solution, $question_explanation){
			$this->question = $question_text;
			$this->solution = $question_solution;
			$this->explanation = $question_explanation;
		}

		abstract protected function show();
	
};

?>