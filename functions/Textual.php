<?php
	include_once baseUrl('functions/Question.php');
	class Textual extends Question{

		public function show(){
			$string= '<li class="list-group-item">'.
					'<div class="container-fluid">'.
						'<small>Question:</small>'.
						'<h4>'. $this->question. '</h4>'.
					'</div>'.
					'<div class="container-fluid">'.
						'<small>Solution</small>'.
						'<textarea class="code">'. $this->solution. '</textarea>'.
					'</div>'.
					'<div class="container-fluid">'.
						'<small>Explanation:</small>'.
						'<pre>'. $this->explanation . '</pre>'.
					'</div>'.
				'</li>';
				return $string; 
		}
	}


?>
