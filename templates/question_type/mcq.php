



<div class="form-group">
  <label for="question_text" class="font-weight-bold">Question:</label>
  <input type="text" name="question_text" id="question_text" class="typeahead form-control form-control-sm" placeholder="Enter the problem question here">
</div> 
<!-- <div class="container" > -->


  <div class="tab-content" >
   <div id= "menu2">
    <div class="row form-group">
      <div class="form-inline col-md-6">
        <input type="radio" name="option" value="a">&nbsp;
        <input type="text" name="option1" id="option1" class="form-control form-control-sm" placeholder="Enter option 1">
      </div>
      <div class="form-inline col-md-6">
        <input type="radio" name="option" value="b">&nbsp;
        <input type="text" name="option2" id="option2" class="form-control form-control-sm" placeholder="Enter option 2">
      </div>
    </div>
    <div class="row form-group">
      <div class="form-inline col-md-6">
        <input type="radio" name="option" value="c">&nbsp;
        <input type="text" name="option3" id="option3" class="form-control form-control-sm" placeholder="Enter option 3">
      </div>
      <div class="form-inline col-md-6">
        <input type="radio" name="option" value="d">&nbsp;
        <input type="text" name="option4" id="option4" class="form-control form-control-sm" placeholder="Enter option 4">
      </div>
    </div>


  <div class="form-group">
    <label for="question_explanation" class="font-weight-bold">Explanation: </label>
    <textarea name="question_explanation" id="question_explanation" class="form-control form-control-sm" placeholder="Enter the explanation here"></textarea>
  </div>
</div>