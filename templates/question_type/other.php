<script>
	function qsa(sel) {
		return Array.apply(null, document.querySelectorAll(sel));
	}
	qsa(".code").forEach(function (editorEl) {
		CodeMirror.fromTextArea(editorEl, {
			lineNumbers: true,
			styleActiveLine: true,
			matchBrackets: true,
			theme: "monokai",
			readOnly: true,
			height: "auto",
		});
	});
</script>
<div class="form-group">
	<label for="question_text" class="font-weight-bold">Question:</label>
	<input type="text" name="question_text" id="question_text" class="typeahead form-control form-control-sm" placeholder="Enter the problem question here">
</div> 
<!-- <div class="container" > -->


	<div class="tab-content" >
		<div id= "menu1">
			<div class="input_fields_wrap card card-body bg-light">
				<button class="btn btn-default add_field_button">Add simple text</button>
				<button class="btn btn-success add_codemirror_button">Add Codemirror</button>
				<button class="btn btn-primary add_ckeditor_button">Add CKeditor</button>
			</div>  
		</div>


		<div class="form-group">
			<label for="question_explanation" class="font-weight-bold">Explanation: </label>
			<textarea name="question_explanation" id="question_explanation" class="form-control form-control-sm" placeholder="Enter the explanation here"></textarea>
		</div>
    </div>



    <script>


    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    var codemirror_add = $(".add_codemirror_button");
    var ckeditor_add = $(".add_ckeditor_button");
    var x = 0; //initlal text box count
    var editor = {};
    $(add_button).click(function(e){ //on add input button click
    	e.preventDefault();
        if(x < max_fields){ //max input box allowed
            $(wrapper).append('<div><textarea type="text" class="form-control" data-type="simple" form-control-sm" name="mytext[]" rows="10"/><a href="#" class="remove_field">Remove</a></div>'); //add input box
        }
            x++; //text box increment        
    });
    $(codemirror_add).click(function(e){ //on add input button click
    	e.preventDefault();
        if(x < max_fields){ //max input box allowed
            $(wrapper).append('<div><textarea class="code" data-type="codemirror" name="mytext[]"></textarea><a href="#" class="remove_field">Remove</a></div>'); //add input box
        }

        editor[x] = CodeMirror.fromTextArea($('.code:last')[0], {
        	mode: "text/x-csharp",
        	lineNumbers: true,
        	styleActiveLine: true,
        	matchBrackets: true,
        	theme: "monokai",
        	height: "auto",
        });
        x++; //text box increment
    });
    $(ckeditor_add).click(function(e){ //on add input button click
    	e.preventDefault();
        if(x < max_fields){ //max input box allowed
            $(wrapper).append('<div><textarea class="ckeditor" data-type="ckeditor" name="mytext[]"></textarea><a href="#" class="remove_field">Remove</a></div>');
            $('.ckeditor:last').ready(function(){
                editor[x] = $('.ckeditor:last').ckeditor();
            });
             //add input box
         }
                     x++; //text box increment
                 });

    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
    	e.preventDefault(); $(this).parent('div').remove(); x--;
    });

</script>