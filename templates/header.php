<?php 
session_start();
if(!isset($_SESSION['user_id'])){
    echo "<script>window.location.replace('php/login.php')</script>";
};
$header='';
$footer='';
$header_index = "";
$footer_index = "";
$header_add_question = "";
$footer_add_question = "";
?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
    <?php include baseUrl('assets/others.php'); ?>
    <?php include baseUrl('assets/codemirror.php'); ?>
    <?php include baseUrl('assets/bootstrap.php'); ?>
    <?php include baseUrl('assets/ckeditor.php'); ?>
    
    <?php echo $header; ?>

</head>

<body>
    <div class="container">
       <?php include baseUrl('php/nav.php');?>
   </div>
   <main role="main" class="container" style="margin-top: 60px;">