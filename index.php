<?php 
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);

$title = 'Index';
?>

<?php require_once('connect.php'); ?>
<?php include_once('templates/header.php'); ?>
<?php require_once baseUrl('functions/Textual.php'); ?>
<?php require_once baseUrl('functions/Logical.php'); ?>
<?php require_once baseUrl('functions/Mcq.php'); ?>
<?php require_once baseUrl('functions/Other.php'); ?>

<?php 
update_question($conn);
if(!isset($_SESSION['questions'])){
	header("Refresh:0");	
}
$result = $_SESSION['questions'];
?>

<?php
function get_content($que_id){
	$data = file_get_contents('assets/code/'.$que_id.'.txt');
	return $data;
}
?>

<?php echo $header_index; ?>



<section class="main-content">
	<ul class="list-group">
		<li class="list-group-item"><a href = "php/add_question.php">Click to add new question</a></li>
		<?php if(isset($result)): ?>
			<?php foreach($result as $row): ?>					
				<?php
				switch($row['type']){
					case "textual":
					$obj = new textual($row['title'], get_content($row['que_id']), $row['explanation']);
					break;


					case "mcq":
					$obj = new mcq($row['title'], get_content($row['que_id']), $row['explanation']);
					break;

					case "logical":
					$obj = new logical($row['title'], get_content($row['que_id']), $row['explanation']);
					break;

					case "other":
					$obj = new other($row['title'], get_content($row['que_id']), $row['explanation']);
					break;
				}
				?>
				<?php echo $obj->show(); ?>

				
			<?php endforeach; ?>
		<?php endif; ?>
	</ul>




</section>



<?php include_once 'templates/footer.php'; ?>