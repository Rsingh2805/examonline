<?php 

ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);

$title = 'My Submissions';
?>

<?php require_once('../connect.php'); ?>
<?php include_once('../templates/header.php'); ?>
<?php 
update_my_question($conn, 'hello');
if(!isset($_SESSION['my_questions'])){
	header("Refresh:0");	
}
$result = $_SESSION['my_questions'];
?>


<?php
function get_content($que_id){
	$data = file_get_contents(getBaseUrl('assets/code/'.$que_id.'.txt'));
	return $data;
}
?>



<section class="main-content">
	<ul class="list-group">
		<li class="list-group-item"><a href = "php/add_question.php">Click to add new question</a></li>
		<?php if(isset($result)): ?>
			<?php foreach($result as $row): ?>	
				<li class="list-group-item">
					<div class="container-fluid">
						<small>Question:</small>
						<h4><?php echo $row['title'] ?></h4>
					</div>
					<div class="container-fluid">
						<small>Solution</small>
						<textarea class="code"><?php echo get_content($row['que_id']); ?></textarea>
					</div>
					<div class="container-fluid">
						<small>Explanation:</small>
						<pre><?php echo $row['explanation'] ?></pre>
					</div>
				</li>
			<?php endforeach; ?>
		<?php endif; ?>
	</ul>




</section>





<?php include_once '../templates/footer.php'; ?>