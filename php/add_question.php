<?php

$title = "Add a question";
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);
?>

<?php require '../connect.php'; ?>
<?php include_once('../templates/header.php'); ?>
<script src="<?php echo getBaseUrl('assets/js/post_submit.js'); ?>"></script>


<div class="container">
  <div class="row">
    <div class="col-md-4">
      <h3>Enter a new question:</h3>
    </div>
    <div class="col-md-4" id="dialog_msg"></div>
  </div>
  <form id="add_question_form" action="<?php echo getBaseUrl('data_handlers/question_handler.php'); ?>" accept-charset="utf-8">

    <div class="row">
      <div class="container-fluid col-md-3 border">
        <?php include 'filters.php'; ?>
      </div>
      <div class= "container-fluid col-md-9 border">
        <div id="tab-content-import"></div> 

      </div>
    </div>
    <div class="row">
      <button type="submit" class="btn btn-default col-md-6" id="add_button" >Add now</button>
      <button type="button" id="refresh" class="btn btn-success col-md-6" disabled="true">Add another question</button>
    </div>
    <div class="row">
        <button onclick="location.href='<?php echo getBaseUrl('php/my_submissions.php'); ?>';" type="button" class="btn btn-primary col-md-12" id="my_questions" >View your submissions</button>
      </div>
  </form>
</div>
<script src = "<?php echo getBaseUrl('assets/js/add_question.js'); ?>"></script>

<?php include_once '../templates/footer.php' ?>

