<?php
include 'basic_data.php';
$conn = mysqli_connect($hostname, $username, $password, $dbname);

if (!$conn) {
	die("Connection failed: " . mysqli_connect_error());
}


function getBaseUrl($string) {
	return "http://localhost/examonline/" . $string;
}
function baseUrl($string) {
	return $_SERVER['DOCUMENT_ROOT']."/examonline/" . $string;
}
function update_question($conn){
	$query = "Select * from question";
	$result = mysqli_query($conn, $query);

	$data = array(); // create a variable to hold the information
	while ($row = mysqli_fetch_assoc($result)){
	  $data[] = $row; // add the row in to the results (data) array
	}

	$_SESSION['questions'] = $data;
}
function update_my_question($conn){
	$query = "Select * from question where user_id=".$_SESSION['user_id'];
	$result = mysqli_query($conn, $query);

	$data = array(); // create a variable to hold the information
	while ($row = mysqli_fetch_assoc($result)){
	  $data[] = $row; // add the row in to the results (data) array
	}

	$_SESSION['my_questions'] = $data;
}
function update_subject($conn){
	$query = "Select * from subject";
	$result = mysqli_query($conn, $query);

	$data = array(); // create a variable to hold the information
	while ($row = mysqli_fetch_assoc($result)){
	  $data[] = $row; // add the row in to the results (data) array
	}

	$_SESSION['subjects'] = $data;
}
?>