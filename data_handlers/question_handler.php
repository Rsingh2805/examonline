<?php
session_start();
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);
include '../DB.php';
include '../connect.php';
$db = new DB();
$tblName = 'question';
if(isset($_REQUEST['req_type']) && !empty($_REQUEST['req_type'])){
    $req_type = $_REQUEST['req_type'];
    switch($req_type){


        case "view":
        if(!empty($_POST['conditions'])&& isset($_POST['conditions'])){
            $records = $db->getRows($tblName, $_POST['conditions']);
        }else{
            $records = $db->getRows($tblName);
        }
        if($records){
            $data['records'] = $db->getRows($tblName);
            $data['status'] = 'OK';
        }else{
            $data['records'] = array();
            $data['status'] = 'ERR';
        }
        echo json_encode($data);
        break;



        case "add":
        if(!empty($_POST)){
            $questionData = array(
                'title' => $_POST['question_text'],
                'explanation' => htmlspecialchars($_POST['question_explanation']),
                'sub_id' => $_POST['question_subject_id'],
                'type' => $_POST['question_type'],
                'user_id' => $_SESSION['user_id'],
            );
            $insert = $db->insert($tblName,$questionData);
            if($insert){
                $data['data'] = $insert;
                $data['status'] = 'OK';
                $data['msg'] = '0';
            }else{
                $data['status'] = 'ERR';
                $data['msg'] = '1';
            }
        }else{
            $data['status'] = 'ERR';
            $data['msg'] = '1';
        }
            //addition of solution in file
        if($_POST['question_type']=='textual'){
            $question_solution = $_POST['question_solution_code'];
        }else if($_POST['question_type']=='mcq'){
            $question_solution = 'a. '. $_POST['option1'] . PHP_EOL .
            'b. '. $_POST['option2'] . PHP_EOL .
            'c. '. $_POST['option3'] . PHP_EOL .
            'd. '. $_POST['option4'] . PHP_EOL .
            'The correct answer is '. $_POST['option'];
        }else if($_POST['question_type']=='logical'){
            $question_solution = $_POST['add_question_page_ta'];
        }else if($_POST['question_type']=='other'){
         $question_solution = $_POST['question_solution_code'];
     };
     $last_id = $db->db->lastInsertId();
     $file_name = baseUrl('assets/code/'.$last_id.'.txt');
     $myfile = fopen("$file_name", "w") or die($file_name."Unable to open file!");
     fwrite($myfile, $question_solution);;
     fclose($myfile);
     update_question($conn);
     update_my_question($conn);
     mysqli_close($conn);

     echo json_encode($data);
     break;


     case "edit":
     if(!empty($_POST['data'])){
        $userData = array(
            'name' => $_POST['data']['name'],
            'email' => $_POST['data']['email'],
            'phone' => $_POST['data']['phone']
        );
        $condition = array('id' => $_POST['data']['id']);
        $update = $db->update($tblName,$userData,$condition);
        if($update){
            $data['status'] = 'OK';
            $data['msg'] = 'User data has been updated successfully.';
        }else{
            $data['status'] = 'ERR';
            $data['msg'] = 'Some problem occurred, please try again.';
        }
    }else{
        $data['status'] = 'ERR';
        $data['msg'] = 'Some problem occurred, please try again.';
    }
    echo json_encode($data);
    break;


    case "delete":
    if(!empty($_POST['id'])){
        $condition = array('id' => $_POST['id']);
        $delete = $db->delete($tblName,$condition);
        if($delete){
            $data['status'] = 'OK';
            $data['msg'] = 'User data has been deleted successfully.';
        }else{
            $data['status'] = 'ERR';
            $data['msg'] = 'Some problem occurred, please try again.';
        }
    }else{
        $data['status'] = 'ERR';
        $data['msg'] = 'Some problem occurred, please try again.';
    }
    echo json_encode($data);
    break;
    default:
    echo '{"status":"INVALID"}';
}
}