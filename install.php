<?php

require "basic_data.php";

try 
{
	$connection = new PDO("mysql:host=$hostname", $username, $password , $options);
	$sql = file_get_contents("init.sql");
	$connection->exec($sql);
	
	echo "Database and table users created successfully.";
}

catch(PDOException $error)
{
	echo $sql . "<br>" . $error->getMessage();
}